﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.d_joueurs = New System.Windows.Forms.Button()
        Me.t_joueurs = New System.Windows.Forms.Button()
        Me.q_joueurs = New System.Windows.Forms.Button()
        Me.Nombre_Joueurs = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'd_joueurs
        '
        Me.d_joueurs.Location = New System.Drawing.Point(108, 175)
        Me.d_joueurs.Name = "d_joueurs"
        Me.d_joueurs.Size = New System.Drawing.Size(75, 23)
        Me.d_joueurs.TabIndex = 0
        Me.d_joueurs.Text = "2 joueurs"
        Me.d_joueurs.UseVisualStyleBackColor = True
        '
        't_joueurs
        '
        Me.t_joueurs.Location = New System.Drawing.Point(294, 175)
        Me.t_joueurs.Name = "t_joueurs"
        Me.t_joueurs.Size = New System.Drawing.Size(75, 23)
        Me.t_joueurs.TabIndex = 1
        Me.t_joueurs.Text = "3 joueurs"
        Me.t_joueurs.UseVisualStyleBackColor = True
        '
        'q_joueurs
        '
        Me.q_joueurs.Location = New System.Drawing.Point(488, 175)
        Me.q_joueurs.Name = "q_joueurs"
        Me.q_joueurs.Size = New System.Drawing.Size(75, 23)
        Me.q_joueurs.TabIndex = 2
        Me.q_joueurs.Text = "4 joueurs"
        Me.q_joueurs.UseVisualStyleBackColor = True
        '
        'Nombre_Joueurs
        '
        Me.Nombre_Joueurs.AutoSize = True
        Me.Nombre_Joueurs.Location = New System.Drawing.Point(81, 129)
        Me.Nombre_Joueurs.Name = "Nombre_Joueurs"
        Me.Nombre_Joueurs.Size = New System.Drawing.Size(102, 13)
        Me.Nombre_Joueurs.TabIndex = 3
        Me.Nombre_Joueurs.Text = "Nombre de joueurs :"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.InitialImage = CType(resources.GetObject("PictureBox1.InitialImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(640, 80)
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.InitialImage = CType(resources.GetObject("PictureBox2.InitialImage"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(13, 203)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(640, 80)
        Me.PictureBox2.TabIndex = 5
        Me.PictureBox2.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(666, 486)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Nombre_Joueurs)
        Me.Controls.Add(Me.q_joueurs)
        Me.Controls.Add(Me.t_joueurs)
        Me.Controls.Add(Me.d_joueurs)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents d_joueurs As Button
    Friend WithEvents t_joueurs As Button
    Friend WithEvents q_joueurs As Button
    Friend WithEvents Nombre_Joueurs As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
End Class
