﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLib
{
    class CalculPoint
    {
        public static int CalculPoint(int placementI, int placementJ)
        {
            int score = 0;
            int score_I = 0;
            int score_J = 0;
            int test_placement_I = placementI;
            int test_placement_J = placementJ;

            if ((placementJ - 1 >= 0) && (pictureboxcasepictureboxcaseplateau[XTuile(IndexTuile), YTuiles(IndexTuile) - 1].GetForme_tuile() != ' '))
            {
                test_placement_I = placementI;
                test_placement_J = placementJ;
                test_placement_J--;

                while ((test_placement_J >= 0) && (pictureboxcaseplateau[test_placement_I, test_placement_J].GetForme_tuile() != ' '))
                {
                    score_J++;
                    test_placement_J--;
                }
                test_placement_I = placementI;
                test_placement_J = placementJ;
            }
            if ((placementJ + 1 <= 19) && (pictureboxcaseplateau[XTuile(IndexTuile), YTuiles(IndexTuile) + 1].GetForme_tuile() != ' '))
            {
                test_placement_I = placementI;
                test_placement_J = placementJ;
                test_placement_J++;

                while ((test_placement_J <= 29) && (pictureboxcaseplateau[test_placement_I, test_placement_J].GetForme_tuile() != ' '))
                {
                    score_J++;
                    test_placement_J--;
                }
                test_placement_I = placementI;
                test_placement_J = placementJ;
            }


            if (score_J != 0)
            {
                score_J++;
            }

            if (score_J == 6)
            {
                score_J = score_J + 6;
            }


            if ((placementI + 1 <= 29) && (pictureboxcaseplateau[XTuile(IndexTuile) + 1, YTuiles(IndexTuile)].GetForme_tuile() != ' '))
            {
                test_placement_I = placementI;
                test_placement_J = placementJ;
                test_placement_I++;

                while ((test_placement_I <= 29) && (pictureboxcaseplateau[test_placement_I, test_placement_J].GetForme_tuile() != ' '))
                {
                    score_I++;
                    test_placement_I--;
                }
                test_placement_I = placementI;
                test_placement_J = placementJ;

                if ((placementI - 1 >= 0) && (pictureboxcaseplateau[XTuile(IndexTuile) - 1, YTuiles(IndexTuile)].GetForme_tuile() != ' '))
                {
                    test_placement_I = placementI;
                    test_placement_J = placementJ;
                    test_placement_I--;

                    while ((test_placement_I >= 0) && (pictureboxcaseplateau[test_placement_I, test_placement_J].GetForme_tuile() != ' '))
                    {
                        score_I++;
                        test_placement_I--;
                    }
                    test_placement_I = placementI;
                    test_placement_J = placementJ;
                }

                if (score_I != 0)
                {
                    score_I++;
                }

                if (score_I == 6)
                {
                    score_I = score_I + 6;
                }

                score = score_I + score_J;

                return score;
            }
        }
    }
}
