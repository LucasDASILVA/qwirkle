﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace QwirkleLib
{

    public class Joueur
    {
        private int TourOrder;
        private int ValeurMain;
        private string Pseudo;
        private int Score;
        // public DateTime NaisJoueur;


        public Joueur(string Pseudo, int Score, int ValeurMain, int TourOrder)//,DateTime Dates)
        {
            this.Pseudo = Pseudo;
            this.Score = Score;
            this.ValeurMain = ValeurMain;
            this.TourOrder = TourOrder;
            //this.NaisJoueur = Dates;


        }



        public void Set_Score(int Score)
        {
            this.Score = Score;
        }

        public int Get_Score()
        {
            return Score;
        }

        public string Get_Name()
        {
            return Pseudo;
        }

        public void Set_Name(string Pseudo)
        {
            this.Pseudo = Pseudo;
        }

        public int Get_ValeurMain()
        {
            return ValeurMain;
        }

        public void Set_ValeurMain(int ValeurMain)
        {
            this.ValeurMain = ValeurMain;
        }

        public int Get_TourOrder()
        {
            return TourOrder;
        }

        public void Set_TourOrder(int TourOrder)
        {
            this.TourOrder = TourOrder;
        }




        // içi attribut de la pioche etc...
    }
}
