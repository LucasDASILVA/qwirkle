﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


namespace QwirkleLib
{
    public class pioche
    {
        private int Qte_tuiles;
        private tuile[] Tab_tuiles;

        // Constructeur basique (Une pioche, c'est une quantité de tuiles --> Soit un tableau de tuiles.)
        public pioche()
        {
            this.Qte_tuiles = 108;
            Forme forme = 0;
            Couleur couleur = 0;

            int index = 0;
            tuile[] Tab_tuiles = new tuile[Qte_tuiles];
            this.Tab_tuiles = Tab_tuiles;
            for(int indexForme = 0;indexForme<6;indexForme++)
            {
                switch (indexForme)
                {

                    case 0:
                        forme = Forme.croix;
                        break;
                    case 1:
                        forme = Forme.carre;
                        break;
                    case 2:
                        forme = Forme.rond;
                        break;
                    case 3:
                        forme = Forme.losange;
                        break;
                    case 4:
                        forme = Forme.trefle;
                        break;
                    case 5:
                        forme = Forme.croix;
                        break;
                }
                for (int indexCouleur = 0;indexCouleur<6;indexCouleur++)
                {

                    switch (indexCouleur)
                    {
                        case 0:
                            couleur = Couleur.jaune;
                            break;
                        case 1:
                            couleur = Couleur.rouge;
                            break;
                        case 2:
                            couleur = Couleur.vert;
                            break;
                        case 3:
                            couleur = Couleur.orange;
                            break;
                        case 4:
                            couleur = Couleur.violet;
                            break;
                        case 5:
                            couleur = Couleur.bleu;
                            break;
                    }
                    for (int indexCopie = 0; indexCopie < 3; indexCopie++,index++)
                    {
                        this.Tab_tuiles[index] = new tuile(forme, couleur);
                    }
                    
                }
            }
            
        }

        // Lorsque le joueur pioche, cette méthode lui retourne une tuile aléatoire dans la pioche.
        public tuile Perte_tuile()
        {
            Thread.Sleep(500);
            Random aleatoire = new Random((int)DateTime.Now.Ticks);
            int nbAleatoire = aleatoire.Next(0, Qte_tuiles);
            tuile tuileChoisie = new tuile(Tab_tuiles[nbAleatoire]);
            for (int index = nbAleatoire; index < Qte_tuiles - 1; index++)
            {
                Tab_tuiles[index] = Tab_tuiles[index + 1];
            }
            Array.Resize(ref Tab_tuiles, Qte_tuiles - 1);
            Qte_tuiles = Tab_tuiles.Length;
            return tuileChoisie;
        }


        public void Gain_tuile(tuile Tuile)
        {
            this.Qte_tuiles = Qte_tuiles + 1;
            Array.Resize(ref Tab_tuiles, Qte_tuiles);
            this.Tab_tuiles[Qte_tuiles - 1] = Tuile;
        }

        public int GetQte_tuiles()
        {
            return this.Qte_tuiles;
        }

        public Couleur GetTab_tuileCouleur(int index)
        {
            tuile Tuile = this.Tab_tuiles[index];
            return Tuile.getCouleur_tuile();
        }

        public Forme GetTab_tuileForme(int index)
        {
            tuile Tuile = this.Tab_tuiles[index];
            return Tuile.getForme_tuile();
        }
    }
}
