﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLib
{
    public enum Forme { etoile, carre ,rond, losange, trefle, croix }
    public enum Couleur { jaune, rouge, vert, orange, violet, bleu }
    public class tuile
    {
        private Forme Forme_tuile;
        private Couleur Couleur_tuile;


        // Constructeur basique --> D'abord la forme puis la couleur.
        public tuile(Forme Forme_tuile, Couleur Couleur_tuile)
        {
            this.Forme_tuile = Forme_tuile;
            this.Couleur_tuile = Couleur_tuile;
        }

        // Constructeur avec une autre tuile (Notamment utile dans la méthode Perte_tuile de la classe Pioche)
        public tuile(tuile Tuile)
        {
            this.Forme_tuile = Tuile.Forme_tuile;
            this.Couleur_tuile = Tuile.Couleur_tuile;
        }

        public Forme getForme_tuile()
        {
            return this.Forme_tuile;
        }

        public Couleur getCouleur_tuile()
        {
            return this.Couleur_tuile;
        }



    }
}
