﻿using System;
using Xunit;
using QwirkleLib;

namespace QwirkleTest
{
    public class PiocheClassTest
    {
        [Fact]
        public void TestPiocheConstructor()
        {
            pioche Pioche = new pioche();
            Assert.Equal(Forme.croix, Pioche.GetTab_tuileForme(0));
            Assert.Equal("vert", Pioche.GetTab_tuileCouleur(2));
        }

        // Problème pour vérifier le bon fonctionnement via les T.U (Impossible de prévoir un résultat si le résultat dépend de l'aléatoire).
        [Fact]
        public void TestPiochePerteTuile()
        {
            tuile[] tabTuile = new tuile[3] { new tuile("etoile", "rouge"), new tuile("caree", "orange"), new tuile("rond", "vert") };
            pioche Pioche = new pioche(3, tabTuile);
            tuile tuilePiochee = new tuile(Pioche.Perte_tuile());
            Assert.Equal(2, Pioche.GetQte_tuiles());

        }


        // Ce test consiste à ajouter une tuile en forme de croix et de couleur violette à une pioche contenant 3 tuiles.
        [Fact]
        public void TestPiocheGainTuile()
        {
            tuile[] tabTuile = new tuile[3] { new tuile("etoile", "rouge"), new tuile("caree", "orange"), new tuile("rond", "vert") };
            pioche Pioche = new pioche(3, tabTuile);
            tuile croix_violet = new tuile("croix", "violet");
            Pioche.Gain_tuile(croix_violet);
            Assert.Equal(4, Pioche.GetQte_tuiles());
            Assert.Equal("croix", Pioche.GetTab_tuileForme(3));
            Assert.Equal("violet", Pioche.GetTab_tuileCouleur(3));
        }
    }
}
