using System;
using Xunit;
using QwirkleLib;

namespace QwirkleTest
{
    public class TuileClassTest
    {
        [Fact]
        public void TestTuileConstructor()
        {
            tuile etoile_rouge = new tuile(Forme.etoile, Couleur.rouge);
            tuile rond_vert = new tuile(Forme.rond, Couleur.vert);
        }

        [Fact]
        public void TestGetCouleur()
        {
            tuile etoile_rouge = new tuile(Forme.etoile, Couleur.rouge);
            Assert.Equal(Couleur.rouge, etoile_rouge.getCouleur_tuile());
        }
        [Fact]
        public void TestGetForme()
        {
            tuile etoile_rouge = new tuile(Forme.etoile, Couleur.rouge);
            Assert.Equal(Forme.etoile, etoile_rouge.getForme_tuile());
        }
    }
}
