﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NbJoueurs
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NbJoueurs))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Date_de_naissance_4 = New System.Windows.Forms.Label()
        Me.Date_de_naissance_3 = New System.Windows.Forms.Label()
        Me.Date_de_naissance_2 = New System.Windows.Forms.Label()
        Me.Date_de_naissance = New System.Windows.Forms.Label()
        Me.DateTimePickerJ4 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePickerJ3 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePickerJ2 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePickerJ1 = New System.Windows.Forms.DateTimePicker()
        Me.Joueur4 = New System.Windows.Forms.TextBox()
        Me.Joueur3 = New System.Windows.Forms.TextBox()
        Me.Joueur2 = New System.Windows.Forms.TextBox()
        Me.Joueur1 = New System.Windows.Forms.TextBox()
        Me.Pseudo_joueur_4 = New System.Windows.Forms.Label()
        Me.Pseudo_joueur_3 = New System.Windows.Forms.Label()
        Me.Pseudo_joueur_2 = New System.Windows.Forms.Label()
        Me.Pseudo_joueur_1 = New System.Windows.Forms.Label()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Nombre_Joueurs = New System.Windows.Forms.Label()
        Me.q_joueurs = New System.Windows.Forms.Button()
        Me.t_joueurs = New System.Windows.Forms.Button()
        Me.d_joueurs = New System.Windows.Forms.Button()
        Me.Boutton_Annuler = New System.Windows.Forms.Button()
        Me.Boutton_Jouer = New System.Windows.Forms.Button()
        Me.Boutton_Retour = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Date_de_naissance_4)
        Me.GroupBox1.Controls.Add(Me.Date_de_naissance_3)
        Me.GroupBox1.Controls.Add(Me.Date_de_naissance_2)
        Me.GroupBox1.Controls.Add(Me.Date_de_naissance)
        Me.GroupBox1.Controls.Add(Me.DateTimePickerJ4)
        Me.GroupBox1.Controls.Add(Me.DateTimePickerJ3)
        Me.GroupBox1.Controls.Add(Me.DateTimePickerJ2)
        Me.GroupBox1.Controls.Add(Me.DateTimePickerJ1)
        Me.GroupBox1.Controls.Add(Me.Joueur4)
        Me.GroupBox1.Controls.Add(Me.Joueur3)
        Me.GroupBox1.Controls.Add(Me.Joueur2)
        Me.GroupBox1.Controls.Add(Me.Joueur1)
        Me.GroupBox1.Controls.Add(Me.Pseudo_joueur_4)
        Me.GroupBox1.Controls.Add(Me.Pseudo_joueur_3)
        Me.GroupBox1.Controls.Add(Me.Pseudo_joueur_2)
        Me.GroupBox1.Controls.Add(Me.Pseudo_joueur_1)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox1.Location = New System.Drawing.Point(94, 189)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(458, 163)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pseudo/Date de naissance"
        '
        'Date_de_naissance_4
        '
        Me.Date_de_naissance_4.AutoSize = True
        Me.Date_de_naissance_4.Location = New System.Drawing.Point(198, 116)
        Me.Date_de_naissance_4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Date_de_naissance_4.Name = "Date_de_naissance_4"
        Me.Date_de_naissance_4.Size = New System.Drawing.Size(102, 13)
        Me.Date_de_naissance_4.TabIndex = 15
        Me.Date_de_naissance_4.Text = "Date de naissance :"
        '
        'Date_de_naissance_3
        '
        Me.Date_de_naissance_3.AutoSize = True
        Me.Date_de_naissance_3.Location = New System.Drawing.Point(198, 90)
        Me.Date_de_naissance_3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Date_de_naissance_3.Name = "Date_de_naissance_3"
        Me.Date_de_naissance_3.Size = New System.Drawing.Size(102, 13)
        Me.Date_de_naissance_3.TabIndex = 14
        Me.Date_de_naissance_3.Text = "Date de naissance :"
        '
        'Date_de_naissance_2
        '
        Me.Date_de_naissance_2.AutoSize = True
        Me.Date_de_naissance_2.Location = New System.Drawing.Point(198, 55)
        Me.Date_de_naissance_2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Date_de_naissance_2.Name = "Date_de_naissance_2"
        Me.Date_de_naissance_2.Size = New System.Drawing.Size(102, 13)
        Me.Date_de_naissance_2.TabIndex = 13
        Me.Date_de_naissance_2.Text = "Date de naissance :"
        '
        'Date_de_naissance
        '
        Me.Date_de_naissance.AutoSize = True
        Me.Date_de_naissance.Location = New System.Drawing.Point(198, 25)
        Me.Date_de_naissance.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Date_de_naissance.Name = "Date_de_naissance"
        Me.Date_de_naissance.Size = New System.Drawing.Size(102, 13)
        Me.Date_de_naissance.TabIndex = 12
        Me.Date_de_naissance.Text = "Date de naissance :"
        '
        'DateTimePickerJ4
        '
        Me.DateTimePickerJ4.Location = New System.Drawing.Point(300, 115)
        Me.DateTimePickerJ4.Margin = New System.Windows.Forms.Padding(2)
        Me.DateTimePickerJ4.Name = "DateTimePickerJ4"
        Me.DateTimePickerJ4.Size = New System.Drawing.Size(151, 20)
        Me.DateTimePickerJ4.TabIndex = 11
        '
        'DateTimePickerJ3
        '
        Me.DateTimePickerJ3.Location = New System.Drawing.Point(300, 83)
        Me.DateTimePickerJ3.Margin = New System.Windows.Forms.Padding(2)
        Me.DateTimePickerJ3.Name = "DateTimePickerJ3"
        Me.DateTimePickerJ3.Size = New System.Drawing.Size(151, 20)
        Me.DateTimePickerJ3.TabIndex = 10
        '
        'DateTimePickerJ2
        '
        Me.DateTimePickerJ2.Location = New System.Drawing.Point(300, 54)
        Me.DateTimePickerJ2.Margin = New System.Windows.Forms.Padding(2)
        Me.DateTimePickerJ2.Name = "DateTimePickerJ2"
        Me.DateTimePickerJ2.Size = New System.Drawing.Size(151, 20)
        Me.DateTimePickerJ2.TabIndex = 9
        '
        'DateTimePickerJ1
        '
        Me.DateTimePickerJ1.Location = New System.Drawing.Point(300, 26)
        Me.DateTimePickerJ1.Margin = New System.Windows.Forms.Padding(2)
        Me.DateTimePickerJ1.Name = "DateTimePickerJ1"
        Me.DateTimePickerJ1.Size = New System.Drawing.Size(151, 20)
        Me.DateTimePickerJ1.TabIndex = 8
        '
        'Joueur4
        '
        Me.Joueur4.Location = New System.Drawing.Point(112, 116)
        Me.Joueur4.Margin = New System.Windows.Forms.Padding(2)
        Me.Joueur4.Name = "Joueur4"
        Me.Joueur4.Size = New System.Drawing.Size(76, 20)
        Me.Joueur4.TabIndex = 7
        '
        'Joueur3
        '
        Me.Joueur3.Location = New System.Drawing.Point(112, 82)
        Me.Joueur3.Margin = New System.Windows.Forms.Padding(2)
        Me.Joueur3.Name = "Joueur3"
        Me.Joueur3.Size = New System.Drawing.Size(76, 20)
        Me.Joueur3.TabIndex = 6
        '
        'Joueur2
        '
        Me.Joueur2.Location = New System.Drawing.Point(112, 53)
        Me.Joueur2.Margin = New System.Windows.Forms.Padding(2)
        Me.Joueur2.Name = "Joueur2"
        Me.Joueur2.Size = New System.Drawing.Size(76, 20)
        Me.Joueur2.TabIndex = 5
        '
        'Joueur1
        '
        Me.Joueur1.Location = New System.Drawing.Point(112, 23)
        Me.Joueur1.Margin = New System.Windows.Forms.Padding(2)
        Me.Joueur1.Name = "Joueur1"
        Me.Joueur1.Size = New System.Drawing.Size(76, 20)
        Me.Joueur1.TabIndex = 4
        '
        'Pseudo_joueur_4
        '
        Me.Pseudo_joueur_4.AutoSize = True
        Me.Pseudo_joueur_4.Location = New System.Drawing.Point(11, 116)
        Me.Pseudo_joueur_4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Pseudo_joueur_4.Name = "Pseudo_joueur_4"
        Me.Pseudo_joueur_4.Size = New System.Drawing.Size(90, 13)
        Me.Pseudo_joueur_4.TabIndex = 3
        Me.Pseudo_joueur_4.Text = "Pseudo joueur 4 :"
        '
        'Pseudo_joueur_3
        '
        Me.Pseudo_joueur_3.AutoSize = True
        Me.Pseudo_joueur_3.Location = New System.Drawing.Point(11, 84)
        Me.Pseudo_joueur_3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Pseudo_joueur_3.Name = "Pseudo_joueur_3"
        Me.Pseudo_joueur_3.Size = New System.Drawing.Size(90, 13)
        Me.Pseudo_joueur_3.TabIndex = 2
        Me.Pseudo_joueur_3.Text = "Pseudo joueur 3 :"
        '
        'Pseudo_joueur_2
        '
        Me.Pseudo_joueur_2.AutoSize = True
        Me.Pseudo_joueur_2.Location = New System.Drawing.Point(11, 55)
        Me.Pseudo_joueur_2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Pseudo_joueur_2.Name = "Pseudo_joueur_2"
        Me.Pseudo_joueur_2.Size = New System.Drawing.Size(90, 13)
        Me.Pseudo_joueur_2.TabIndex = 1
        Me.Pseudo_joueur_2.Text = "Pseudo joueur 2 :"
        '
        'Pseudo_joueur_1
        '
        Me.Pseudo_joueur_1.AutoSize = True
        Me.Pseudo_joueur_1.Location = New System.Drawing.Point(11, 25)
        Me.Pseudo_joueur_1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Pseudo_joueur_1.Name = "Pseudo_joueur_1"
        Me.Pseudo_joueur_1.Size = New System.Drawing.Size(90, 13)
        Me.Pseudo_joueur_1.TabIndex = 0
        Me.Pseudo_joueur_1.Text = "Pseudo joueur 1 :"
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(508, 359)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(66, 58)
        Me.PictureBox7.TabIndex = 25
        Me.PictureBox7.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(452, 414)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(61, 59)
        Me.PictureBox6.TabIndex = 24
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(508, 414)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(66, 59)
        Me.PictureBox5.TabIndex = 23
        Me.PictureBox5.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(568, 289)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(75, 184)
        Me.PictureBox4.TabIndex = 22
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(1, 161)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(65, 65)
        Me.PictureBox3.TabIndex = 21
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(73, 388)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(65, 61)
        Me.PictureBox2.TabIndex = 20
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1, -2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(642, 82)
        Me.PictureBox1.TabIndex = 19
        Me.PictureBox1.TabStop = False
        '
        'Nombre_Joueurs
        '
        Me.Nombre_Joueurs.AutoSize = True
        Me.Nombre_Joueurs.Location = New System.Drawing.Point(70, 115)
        Me.Nombre_Joueurs.Name = "Nombre_Joueurs"
        Me.Nombre_Joueurs.Size = New System.Drawing.Size(102, 13)
        Me.Nombre_Joueurs.TabIndex = 18
        Me.Nombre_Joueurs.Text = "Nombre de joueurs :"
        '
        'q_joueurs
        '
        Me.q_joueurs.Location = New System.Drawing.Point(477, 151)
        Me.q_joueurs.Name = "q_joueurs"
        Me.q_joueurs.Size = New System.Drawing.Size(75, 23)
        Me.q_joueurs.TabIndex = 17
        Me.q_joueurs.Text = "4 joueurs"
        Me.q_joueurs.UseVisualStyleBackColor = True
        '
        't_joueurs
        '
        Me.t_joueurs.Location = New System.Drawing.Point(283, 151)
        Me.t_joueurs.Name = "t_joueurs"
        Me.t_joueurs.Size = New System.Drawing.Size(75, 23)
        Me.t_joueurs.TabIndex = 16
        Me.t_joueurs.Text = "3 joueurs"
        Me.t_joueurs.UseVisualStyleBackColor = True
        '
        'd_joueurs
        '
        Me.d_joueurs.Location = New System.Drawing.Point(94, 151)
        Me.d_joueurs.Name = "d_joueurs"
        Me.d_joueurs.Size = New System.Drawing.Size(75, 23)
        Me.d_joueurs.TabIndex = 15
        Me.d_joueurs.Text = "2 joueurs"
        Me.d_joueurs.UseVisualStyleBackColor = True
        '
        'Boutton_Annuler
        '
        Me.Boutton_Annuler.Location = New System.Drawing.Point(264, 406)
        Me.Boutton_Annuler.Name = "Boutton_Annuler"
        Me.Boutton_Annuler.Size = New System.Drawing.Size(75, 23)
        Me.Boutton_Annuler.TabIndex = 28
        Me.Boutton_Annuler.Text = "Annuler"
        Me.Boutton_Annuler.UseVisualStyleBackColor = True
        '
        'Boutton_Jouer
        '
        Me.Boutton_Jouer.Location = New System.Drawing.Point(360, 406)
        Me.Boutton_Jouer.Name = "Boutton_Jouer"
        Me.Boutton_Jouer.Size = New System.Drawing.Size(75, 23)
        Me.Boutton_Jouer.TabIndex = 29
        Me.Boutton_Jouer.Text = "Jouer"
        Me.Boutton_Jouer.UseVisualStyleBackColor = True
        '
        'Boutton_Retour
        '
        Me.Boutton_Retour.Location = New System.Drawing.Point(166, 406)
        Me.Boutton_Retour.Name = "Boutton_Retour"
        Me.Boutton_Retour.Size = New System.Drawing.Size(75, 23)
        Me.Boutton_Retour.TabIndex = 30
        Me.Boutton_Retour.Text = "Menu"
        Me.Boutton_Retour.UseVisualStyleBackColor = True
        '
        'NbJoueurs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 468)
        Me.Controls.Add(Me.Boutton_Retour)
        Me.Controls.Add(Me.Boutton_Jouer)
        Me.Controls.Add(Me.Boutton_Annuler)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Nombre_Joueurs)
        Me.Controls.Add(Me.q_joueurs)
        Me.Controls.Add(Me.t_joueurs)
        Me.Controls.Add(Me.d_joueurs)
        Me.Name = "NbJoueurs"
        Me.Text = "ChoixNbJoueurs"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Date_de_naissance_4 As Label
    Friend WithEvents Date_de_naissance_3 As Label
    Friend WithEvents Date_de_naissance_2 As Label
    Friend WithEvents Date_de_naissance As Label
    Friend WithEvents DateTimePickerJ4 As DateTimePicker
    Friend WithEvents DateTimePickerJ3 As DateTimePicker
    Friend WithEvents DateTimePickerJ2 As DateTimePicker
    Friend WithEvents DateTimePickerJ1 As DateTimePicker
    Friend WithEvents Joueur4 As TextBox
    Friend WithEvents Joueur3 As TextBox
    Friend WithEvents Joueur2 As TextBox
    Friend WithEvents Joueur1 As TextBox
    Friend WithEvents Pseudo_joueur_4 As Label
    Friend WithEvents Pseudo_joueur_3 As Label
    Friend WithEvents Pseudo_joueur_2 As Label
    Friend WithEvents Pseudo_joueur_1 As Label
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Nombre_Joueurs As Label
    Friend WithEvents q_joueurs As Button
    Friend WithEvents t_joueurs As Button
    Friend WithEvents d_joueurs As Button
    Friend WithEvents Boutton_Annuler As Button
    Friend WithEvents Boutton_Jouer As Button
    Friend WithEvents Boutton_Retour As Button
End Class
