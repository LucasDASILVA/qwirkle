﻿Imports QwirkleLib
Imports QwirkleVB

Public Class NbJoueurs
    Public NBJoueurSelect As Integer 'Nombre de Joueur Selectionné
    Dim Pseudo As String
    Private Sub NbJoueurs_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Joueur1.Enabled = True
        Joueur2.Enabled = True
        Joueur3.Enabled = False
        Joueur4.Enabled = False

        DateTimePickerJ1.Enabled = True
        DateTimePickerJ2.Enabled = True
        DateTimePickerJ3.Enabled = False
        DateTimePickerJ4.Enabled = False

    End Sub

    Private Sub d_joueurs_Click(sender As Object, e As EventArgs) Handles d_joueurs.Click

        d_joueurs.Enabled = True
        t_joueurs.Enabled = False
        q_joueurs.Enabled = False

        Joueur1.Enabled = True
        Joueur2.Enabled = True
        Joueur3.Enabled = False
        Joueur4.Enabled = False

        DateTimePickerJ1.Enabled = True
        DateTimePickerJ2.Enabled = True
        DateTimePickerJ3.Enabled = False
        DateTimePickerJ4.Enabled = False

        NBJoueurSelect = 2

    End Sub

    Private Sub t_joueurs_Click(sender As Object, e As EventArgs) Handles t_joueurs.Click
        d_joueurs.Enabled = False
        t_joueurs.Enabled = True
        q_joueurs.Enabled = False

        Joueur1.Enabled = True
        Joueur2.Enabled = True
        Joueur3.Enabled = True
        Joueur4.Enabled = False

        DateTimePickerJ1.Enabled = True
        DateTimePickerJ2.Enabled = True
        DateTimePickerJ3.Enabled = True
        DateTimePickerJ4.Enabled = False

        NBJoueurSelect = 3

    End Sub

    Private Sub q_joueurs_Click(sender As Object, e As EventArgs) Handles q_joueurs.Click

        d_joueurs.Enabled = False
        t_joueurs.Enabled = False
        q_joueurs.Enabled = True

        Joueur1.Enabled = True
        Joueur2.Enabled = True
        Joueur3.Enabled = True
        Joueur4.Enabled = True

        DateTimePickerJ1.Enabled = True
        DateTimePickerJ2.Enabled = True
        DateTimePickerJ3.Enabled = True
        DateTimePickerJ4.Enabled = True

        NBJoueurSelect = 4

    End Sub

    Private Sub Boutton_Annuler_Click(sender As Object, e As EventArgs) Handles Boutton_Annuler.Click

        d_joueurs.Enabled = True
        t_joueurs.Enabled = True
        q_joueurs.Enabled = True

        Joueur1.Enabled = True
        Joueur2.Enabled = True
        Joueur3.Enabled = False
        Joueur4.Enabled = False

        DateTimePickerJ1.Enabled = True
        DateTimePickerJ2.Enabled = True
        DateTimePickerJ3.Enabled = False
        DateTimePickerJ4.Enabled = False

    End Sub


    Private Sub Boutton_Retour_Click(sender As Object, e As EventArgs) Handles Boutton_Retour.Click
        Me.Hide()
        Accueil.Show()
    End Sub


    Private Sub Boutton_Jouer_Click(sender As Object, e As EventArgs) Handles Boutton_Jouer.Click
        If Joueur1.Enabled = True And Joueur1.Text = "" Or Joueur2.Enabled = True And Joueur2.Text = "" Or Joueur3.Enabled = True And Joueur3.Text = "" Or Joueur4.Enabled = True And Joueur4.Text = "" Then
            MsgBox("Information incomplète")
        Else

            Me.Hide()
            ObjetJoueur.Joueur1.Set_Name(Joueur1.Text)
            ObjetJoueur.Joueur2.Set_Name(Joueur2.Text)
            ObjetJoueur.Joueur3.Set_Name(Joueur3.Text)
            ObjetJoueur.Joueur4.Set_Name(Joueur4.Text)
            'J1 = New Joueur(Joueur1.Text)
            'J2 = New Joueur(Joueur2.Text)

            Plateau.ShowDialog() 'affichage de l'interface de jeu

            'J4 = New Joueur(Joueur4.Text)     Dans les parenthèses mettre (Form1.TextBox.Text)
            'J3 = New Joueur(Joueur3.Text)     Mettre c'est 4 lignes dans le form du plateau et changer "form2" par le nom du form
        End If
    End Sub

End Class


