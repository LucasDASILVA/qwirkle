﻿Imports System.Windows.Forms
Imports QwirkleLib
Imports QwirkleVB



Public Class Dialogscore
    Dim ValeurMainString As String 'Valeur initiale non enregistré 
    Dim ValeurMain As Integer 'Valeur du score de la main du joueur Enregistré
    Dim i As Integer = 0 'index pour la list des joueurs
    Dim restriction() As Char = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"} 'Restriction des charactères possible sur la ValeurMain
    Dim lst As New List(Of Joueur) From {Joueur1, Joueur2, Joueur3, Joueur4} 'Création d'une liste de joueur
    Dim TriFini As Integer 'Variable pour savoir si une modification à était faite

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        ValeurMainString = TextBox1.Text

        Me.DialogResult = System.Windows.Forms.DialogResult.OK

        If ValeurMainString = "" Then 'interdiction de ne pas introduire de valeur

        Else
            ValeurMain = CInt(ValeurMainString)

            Select Case Plateau.Order 'switch entre chaque joueur introduit pour leur score de main
                Case 1
                    Joueur1.Set_ValeurMain(ValeurMain)
                   ' MsgBox(Joueur1.Get_ValeurMain)


                Case 2
                    Joueur2.Set_ValeurMain(ValeurMain)
                    ' MsgBox(Joueur2.Get_ValeurMain)

                    If (NbJoueurs.NBJoueurSelect = 2) Then 'décision suivant le nombre de joueur introduit pour un test adapté
                        If Joueur1.Get_ValeurMain >= Joueur2.Get_ValeurMain Then 'test plus rapide qu'un algo de tri à bulle
                            Joueur1.Set_TourOrder(1)
                            Joueur2.Set_TourOrder(2)
                        Else
                            Joueur1.Set_TourOrder(2)
                            Joueur2.Set_TourOrder(1)
                        End If
                    End If
                Case 3
                    Joueur3.Set_ValeurMain(ValeurMain)
                    ' MsgBox(Joueur3.Get_ValeurMain)

                    '  If (NbJoueurs.NBJoueurSelect = 3) Then

                    'Do While TriFini <> 2 'continu tant que le test n'a rien changé sur tout le classement
                    'For Each item As Joueur In lst 'parcour la liste de joueur
                    'If i < 2 Then 'corrige l'erreur de la derniere valeur qui n'a pas de valeur lst(i + 1) comparative
                    'If lst(i).Get_ValeurMain < lst(i + 1).Get_ValeurMain Then
                    'lst(i + 1).Set_TourOrder(-1) 'décrémentation dans le classement de la valeur supérieur
                    'lst(i).Set_TourOrder(+1) 'incrémentation dans le classement de la valeur inférieur
                    'i = i + 1
                    'TriFini = 0 'Test effectué donc réinitialisation du test de fin
                    'Else
                    'TriFini = TriFini + 1 'pas de changement de classement donc incrémentation de la variable

                    'End If
                    ' End If
                    '      Next


                    '              Loop

                    'End If
                    '  Case 4

                    ' Joueur4.Set_ValeurMain(ValeurMain)
                    ' MsgBox(Joueur4.Get_ValeurMain)
                    '  If (NbJoueurs.NBJoueurSelect = 4) Then

                    ' Do While TriFini <> 3 'continu tant que le test n'a rien changé sur tout le classement
                    'For Each item As Joueur In lst 'parcour la liste de joueur
                    ' If i <> 3 Then 'corrige l'erreur de la derniere valeur qui n'a pas de valeur lst(i + 1) comparative
                    '    If lst(i).Get_ValeurMain < lst(i + 1).Get_ValeurMain Then
                    '   lst(i).Set_TourOrder(i + 2) 'décrémentation dans le classement de la valeur supérieur
                    '        lst(i + 1).Set_TourOrder(i + 1) 'incrémentation dans le classement de la valeur inférieur

                    '  TriFini = 0 'Test effectué donc réinitialisation du test de fin
                    'Else
                    'TriFini = TriFini + 1 'pas de changement de classement donc incrémentation de la variable

                    'End If
                    '  i = i + 1
                    ' End If
                    '  Next
                    'i = 0
                    ' TriFini = 0


                    ' Loop
                    ' End If
            End Select

        End If
        If Plateau.Order <> NbJoueurs.NBJoueurSelect Then 'décision pour commencer le switch par le joueur 1 jusqu'au nombre de joueur introduit
            Plateau.Order = Plateau.Order + 1
            '
            Plateau.nbstart = True

        End If
        Me.Close() 'fermeture de la boite de dialogue
        '

    End Sub



    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox1.KeyPress
        If Not restriction.Contains(e.KeyChar) And Not Asc(e.KeyChar) = 8 Then 'autorisation de la touche effacer
            e.Handled = True
        End If
    End Sub

    Private Sub Dialogscore_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox1.MaxLength = 2 'Limitation de la taille du score à 2 chiffres
    End Sub
End Class



