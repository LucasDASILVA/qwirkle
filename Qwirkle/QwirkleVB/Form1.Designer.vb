﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.PictureBoxCasePlateau22 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau12 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau21 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau11 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxMainJoueur6 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxMainJoueur5 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxMainJoueur4 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxMainJoueur3 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxMainJoueur2 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxMainJoueur1 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxEchange = New System.Windows.Forms.PictureBox()
        Me.PictureBoxDechet1 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau31 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau41 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau51 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau32 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau42 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau52 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau54 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau44 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau34 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau53 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau43 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau33 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau24 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau14 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau23 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau13 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau55 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau45 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau35 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau25 = New System.Windows.Forms.PictureBox()
        Me.PictureBoxCasePlateau15 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LabelX = New System.Windows.Forms.Label()
        Me.LabelY = New System.Windows.Forms.Label()
        Me.BtnValider = New System.Windows.Forms.Button()
        Me.BtnAnnuler = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        CType(Me.PictureBoxCasePlateau22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxMainJoueur6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxMainJoueur5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxMainJoueur4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxMainJoueur3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxMainJoueur2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxMainJoueur1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxEchange, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxDechet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBoxCasePlateau15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBoxCasePlateau22
        '
        Me.PictureBoxCasePlateau22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau22.Location = New System.Drawing.Point(281, 138)
        Me.PictureBoxCasePlateau22.Name = "PictureBoxCasePlateau22"
        Me.PictureBoxCasePlateau22.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau22.TabIndex = 9
        Me.PictureBoxCasePlateau22.TabStop = False
        '
        'PictureBoxCasePlateau12
        '
        Me.PictureBoxCasePlateau12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau12.Location = New System.Drawing.Point(188, 138)
        Me.PictureBoxCasePlateau12.Name = "PictureBoxCasePlateau12"
        Me.PictureBoxCasePlateau12.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau12.TabIndex = 8
        Me.PictureBoxCasePlateau12.TabStop = False
        '
        'PictureBoxCasePlateau21
        '
        Me.PictureBoxCasePlateau21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau21.Location = New System.Drawing.Point(281, 50)
        Me.PictureBoxCasePlateau21.Name = "PictureBoxCasePlateau21"
        Me.PictureBoxCasePlateau21.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau21.TabIndex = 7
        Me.PictureBoxCasePlateau21.TabStop = False
        '
        'PictureBoxCasePlateau11
        '
        Me.PictureBoxCasePlateau11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau11.Location = New System.Drawing.Point(188, 50)
        Me.PictureBoxCasePlateau11.Name = "PictureBoxCasePlateau11"
        Me.PictureBoxCasePlateau11.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau11.TabIndex = 6
        Me.PictureBoxCasePlateau11.TabStop = False
        '
        'PictureBoxMainJoueur6
        '
        Me.PictureBoxMainJoueur6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxMainJoueur6.Location = New System.Drawing.Point(775, 519)
        Me.PictureBoxMainJoueur6.Name = "PictureBoxMainJoueur6"
        Me.PictureBoxMainJoueur6.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxMainJoueur6.TabIndex = 5
        Me.PictureBoxMainJoueur6.TabStop = False
        '
        'PictureBoxMainJoueur5
        '
        Me.PictureBoxMainJoueur5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxMainJoueur5.Location = New System.Drawing.Point(687, 519)
        Me.PictureBoxMainJoueur5.Name = "PictureBoxMainJoueur5"
        Me.PictureBoxMainJoueur5.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxMainJoueur5.TabIndex = 4
        Me.PictureBoxMainJoueur5.TabStop = False
        '
        'PictureBoxMainJoueur4
        '
        Me.PictureBoxMainJoueur4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxMainJoueur4.Location = New System.Drawing.Point(595, 519)
        Me.PictureBoxMainJoueur4.Name = "PictureBoxMainJoueur4"
        Me.PictureBoxMainJoueur4.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxMainJoueur4.TabIndex = 3
        Me.PictureBoxMainJoueur4.TabStop = False
        '
        'PictureBoxMainJoueur3
        '
        Me.PictureBoxMainJoueur3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxMainJoueur3.Location = New System.Drawing.Point(502, 519)
        Me.PictureBoxMainJoueur3.Name = "PictureBoxMainJoueur3"
        Me.PictureBoxMainJoueur3.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxMainJoueur3.TabIndex = 2
        Me.PictureBoxMainJoueur3.TabStop = False
        '
        'PictureBoxMainJoueur2
        '
        Me.PictureBoxMainJoueur2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxMainJoueur2.Location = New System.Drawing.Point(414, 519)
        Me.PictureBoxMainJoueur2.Name = "PictureBoxMainJoueur2"
        Me.PictureBoxMainJoueur2.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxMainJoueur2.TabIndex = 1
        Me.PictureBoxMainJoueur2.TabStop = False
        '
        'PictureBoxMainJoueur1
        '
        Me.PictureBoxMainJoueur1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxMainJoueur1.Location = New System.Drawing.Point(325, 519)
        Me.PictureBoxMainJoueur1.Name = "PictureBoxMainJoueur1"
        Me.PictureBoxMainJoueur1.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxMainJoueur1.TabIndex = 0
        Me.PictureBoxMainJoueur1.TabStop = False
        '
        'PictureBoxEchange
        '
        Me.PictureBoxEchange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxEchange.Location = New System.Drawing.Point(728, 428)
        Me.PictureBoxEchange.Name = "PictureBoxEchange"
        Me.PictureBoxEchange.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxEchange.TabIndex = 10
        Me.PictureBoxEchange.TabStop = False
        '
        'PictureBoxDechet1
        '
        Me.PictureBoxDechet1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxDechet1.Location = New System.Drawing.Point(19, 57)
        Me.PictureBoxDechet1.Name = "PictureBoxDechet1"
        Me.PictureBoxDechet1.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxDechet1.TabIndex = 11
        Me.PictureBoxDechet1.TabStop = False
        '
        'PictureBoxCasePlateau31
        '
        Me.PictureBoxCasePlateau31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau31.Location = New System.Drawing.Point(377, 50)
        Me.PictureBoxCasePlateau31.Name = "PictureBoxCasePlateau31"
        Me.PictureBoxCasePlateau31.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau31.TabIndex = 12
        Me.PictureBoxCasePlateau31.TabStop = False
        '
        'PictureBoxCasePlateau41
        '
        Me.PictureBoxCasePlateau41.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau41.Location = New System.Drawing.Point(469, 50)
        Me.PictureBoxCasePlateau41.Name = "PictureBoxCasePlateau41"
        Me.PictureBoxCasePlateau41.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau41.TabIndex = 13
        Me.PictureBoxCasePlateau41.TabStop = False
        '
        'PictureBoxCasePlateau51
        '
        Me.PictureBoxCasePlateau51.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau51.Location = New System.Drawing.Point(562, 50)
        Me.PictureBoxCasePlateau51.Name = "PictureBoxCasePlateau51"
        Me.PictureBoxCasePlateau51.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau51.TabIndex = 14
        Me.PictureBoxCasePlateau51.TabStop = False
        '
        'PictureBoxCasePlateau32
        '
        Me.PictureBoxCasePlateau32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau32.Location = New System.Drawing.Point(377, 138)
        Me.PictureBoxCasePlateau32.Name = "PictureBoxCasePlateau32"
        Me.PictureBoxCasePlateau32.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau32.TabIndex = 15
        Me.PictureBoxCasePlateau32.TabStop = False
        '
        'PictureBoxCasePlateau42
        '
        Me.PictureBoxCasePlateau42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau42.Location = New System.Drawing.Point(469, 138)
        Me.PictureBoxCasePlateau42.Name = "PictureBoxCasePlateau42"
        Me.PictureBoxCasePlateau42.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau42.TabIndex = 16
        Me.PictureBoxCasePlateau42.TabStop = False
        '
        'PictureBoxCasePlateau52
        '
        Me.PictureBoxCasePlateau52.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau52.Location = New System.Drawing.Point(562, 138)
        Me.PictureBoxCasePlateau52.Name = "PictureBoxCasePlateau52"
        Me.PictureBoxCasePlateau52.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau52.TabIndex = 17
        Me.PictureBoxCasePlateau52.TabStop = False
        '
        'PictureBoxCasePlateau54
        '
        Me.PictureBoxCasePlateau54.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau54.Location = New System.Drawing.Point(562, 318)
        Me.PictureBoxCasePlateau54.Name = "PictureBoxCasePlateau54"
        Me.PictureBoxCasePlateau54.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau54.TabIndex = 27
        Me.PictureBoxCasePlateau54.TabStop = False
        '
        'PictureBoxCasePlateau44
        '
        Me.PictureBoxCasePlateau44.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau44.Location = New System.Drawing.Point(469, 318)
        Me.PictureBoxCasePlateau44.Name = "PictureBoxCasePlateau44"
        Me.PictureBoxCasePlateau44.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau44.TabIndex = 26
        Me.PictureBoxCasePlateau44.TabStop = False
        '
        'PictureBoxCasePlateau34
        '
        Me.PictureBoxCasePlateau34.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau34.Location = New System.Drawing.Point(377, 318)
        Me.PictureBoxCasePlateau34.Name = "PictureBoxCasePlateau34"
        Me.PictureBoxCasePlateau34.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau34.TabIndex = 25
        Me.PictureBoxCasePlateau34.TabStop = False
        '
        'PictureBoxCasePlateau53
        '
        Me.PictureBoxCasePlateau53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau53.Location = New System.Drawing.Point(562, 230)
        Me.PictureBoxCasePlateau53.Name = "PictureBoxCasePlateau53"
        Me.PictureBoxCasePlateau53.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau53.TabIndex = 24
        Me.PictureBoxCasePlateau53.TabStop = False
        '
        'PictureBoxCasePlateau43
        '
        Me.PictureBoxCasePlateau43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau43.Location = New System.Drawing.Point(469, 230)
        Me.PictureBoxCasePlateau43.Name = "PictureBoxCasePlateau43"
        Me.PictureBoxCasePlateau43.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau43.TabIndex = 23
        Me.PictureBoxCasePlateau43.TabStop = False
        '
        'PictureBoxCasePlateau33
        '
        Me.PictureBoxCasePlateau33.BackColor = System.Drawing.Color.Silver
        Me.PictureBoxCasePlateau33.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau33.Location = New System.Drawing.Point(377, 230)
        Me.PictureBoxCasePlateau33.Name = "PictureBoxCasePlateau33"
        Me.PictureBoxCasePlateau33.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau33.TabIndex = 22
        Me.PictureBoxCasePlateau33.TabStop = False
        '
        'PictureBoxCasePlateau24
        '
        Me.PictureBoxCasePlateau24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau24.Location = New System.Drawing.Point(281, 318)
        Me.PictureBoxCasePlateau24.Name = "PictureBoxCasePlateau24"
        Me.PictureBoxCasePlateau24.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau24.TabIndex = 21
        Me.PictureBoxCasePlateau24.TabStop = False
        '
        'PictureBoxCasePlateau14
        '
        Me.PictureBoxCasePlateau14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau14.Location = New System.Drawing.Point(188, 318)
        Me.PictureBoxCasePlateau14.Name = "PictureBoxCasePlateau14"
        Me.PictureBoxCasePlateau14.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau14.TabIndex = 20
        Me.PictureBoxCasePlateau14.TabStop = False
        '
        'PictureBoxCasePlateau23
        '
        Me.PictureBoxCasePlateau23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau23.Location = New System.Drawing.Point(281, 230)
        Me.PictureBoxCasePlateau23.Name = "PictureBoxCasePlateau23"
        Me.PictureBoxCasePlateau23.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau23.TabIndex = 19
        Me.PictureBoxCasePlateau23.TabStop = False
        '
        'PictureBoxCasePlateau13
        '
        Me.PictureBoxCasePlateau13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau13.Location = New System.Drawing.Point(188, 230)
        Me.PictureBoxCasePlateau13.Name = "PictureBoxCasePlateau13"
        Me.PictureBoxCasePlateau13.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau13.TabIndex = 18
        Me.PictureBoxCasePlateau13.TabStop = False
        '
        'PictureBoxCasePlateau55
        '
        Me.PictureBoxCasePlateau55.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau55.Location = New System.Drawing.Point(562, 401)
        Me.PictureBoxCasePlateau55.Name = "PictureBoxCasePlateau55"
        Me.PictureBoxCasePlateau55.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau55.TabIndex = 32
        Me.PictureBoxCasePlateau55.TabStop = False
        '
        'PictureBoxCasePlateau45
        '
        Me.PictureBoxCasePlateau45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau45.Location = New System.Drawing.Point(469, 401)
        Me.PictureBoxCasePlateau45.Name = "PictureBoxCasePlateau45"
        Me.PictureBoxCasePlateau45.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau45.TabIndex = 31
        Me.PictureBoxCasePlateau45.TabStop = False
        '
        'PictureBoxCasePlateau35
        '
        Me.PictureBoxCasePlateau35.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau35.Location = New System.Drawing.Point(377, 401)
        Me.PictureBoxCasePlateau35.Name = "PictureBoxCasePlateau35"
        Me.PictureBoxCasePlateau35.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau35.TabIndex = 30
        Me.PictureBoxCasePlateau35.TabStop = False
        '
        'PictureBoxCasePlateau25
        '
        Me.PictureBoxCasePlateau25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau25.Location = New System.Drawing.Point(281, 401)
        Me.PictureBoxCasePlateau25.Name = "PictureBoxCasePlateau25"
        Me.PictureBoxCasePlateau25.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau25.TabIndex = 29
        Me.PictureBoxCasePlateau25.TabStop = False
        '
        'PictureBoxCasePlateau15
        '
        Me.PictureBoxCasePlateau15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBoxCasePlateau15.Location = New System.Drawing.Point(188, 401)
        Me.PictureBoxCasePlateau15.Name = "PictureBoxCasePlateau15"
        Me.PictureBoxCasePlateau15.Size = New System.Drawing.Size(69, 68)
        Me.PictureBoxCasePlateau15.TabIndex = 28
        Me.PictureBoxCasePlateau15.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(943, 188)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(158, 17)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "Nom De La Case Jouée"
        '
        'LabelX
        '
        Me.LabelX.AutoSize = True
        Me.LabelX.Location = New System.Drawing.Point(958, 215)
        Me.LabelX.Name = "LabelX"
        Me.LabelX.Size = New System.Drawing.Size(17, 17)
        Me.LabelX.TabIndex = 34
        Me.LabelX.Text = "X"
        '
        'LabelY
        '
        Me.LabelY.AutoSize = True
        Me.LabelY.Location = New System.Drawing.Point(1004, 215)
        Me.LabelY.Name = "LabelY"
        Me.LabelY.Size = New System.Drawing.Size(17, 17)
        Me.LabelY.TabIndex = 35
        Me.LabelY.Text = "Y"
        '
        'BtnValider
        '
        Me.BtnValider.Location = New System.Drawing.Point(775, 246)
        Me.BtnValider.Name = "BtnValider"
        Me.BtnValider.Size = New System.Drawing.Size(116, 31)
        Me.BtnValider.TabIndex = 36
        Me.BtnValider.Text = "Valider"
        Me.BtnValider.UseVisualStyleBackColor = True
        '
        'BtnAnnuler
        '
        Me.BtnAnnuler.Location = New System.Drawing.Point(775, 298)
        Me.BtnAnnuler.Name = "BtnAnnuler"
        Me.BtnAnnuler.Size = New System.Drawing.Size(116, 31)
        Me.BtnAnnuler.TabIndex = 37
        Me.BtnAnnuler.Text = "Annuler"
        Me.BtnAnnuler.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(803, 428)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(293, 17)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Ceci est la poubelle pour l'echange des tuiles"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(282, 17)
        Me.Label3.TabIndex = 39
        Me.Label3.Text = "Ceci symbolise ce que la poubelle récupere"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1662, 981)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BtnAnnuler)
        Me.Controls.Add(Me.BtnValider)
        Me.Controls.Add(Me.LabelY)
        Me.Controls.Add(Me.LabelX)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBoxCasePlateau55)
        Me.Controls.Add(Me.PictureBoxCasePlateau45)
        Me.Controls.Add(Me.PictureBoxCasePlateau35)
        Me.Controls.Add(Me.PictureBoxCasePlateau25)
        Me.Controls.Add(Me.PictureBoxCasePlateau15)
        Me.Controls.Add(Me.PictureBoxCasePlateau54)
        Me.Controls.Add(Me.PictureBoxCasePlateau44)
        Me.Controls.Add(Me.PictureBoxCasePlateau34)
        Me.Controls.Add(Me.PictureBoxCasePlateau53)
        Me.Controls.Add(Me.PictureBoxCasePlateau43)
        Me.Controls.Add(Me.PictureBoxCasePlateau33)
        Me.Controls.Add(Me.PictureBoxCasePlateau24)
        Me.Controls.Add(Me.PictureBoxCasePlateau14)
        Me.Controls.Add(Me.PictureBoxCasePlateau23)
        Me.Controls.Add(Me.PictureBoxCasePlateau13)
        Me.Controls.Add(Me.PictureBoxCasePlateau52)
        Me.Controls.Add(Me.PictureBoxCasePlateau42)
        Me.Controls.Add(Me.PictureBoxCasePlateau32)
        Me.Controls.Add(Me.PictureBoxCasePlateau51)
        Me.Controls.Add(Me.PictureBoxCasePlateau41)
        Me.Controls.Add(Me.PictureBoxCasePlateau31)
        Me.Controls.Add(Me.PictureBoxDechet1)
        Me.Controls.Add(Me.PictureBoxEchange)
        Me.Controls.Add(Me.PictureBoxCasePlateau22)
        Me.Controls.Add(Me.PictureBoxCasePlateau12)
        Me.Controls.Add(Me.PictureBoxCasePlateau21)
        Me.Controls.Add(Me.PictureBoxCasePlateau11)
        Me.Controls.Add(Me.PictureBoxMainJoueur6)
        Me.Controls.Add(Me.PictureBoxMainJoueur5)
        Me.Controls.Add(Me.PictureBoxMainJoueur4)
        Me.Controls.Add(Me.PictureBoxMainJoueur3)
        Me.Controls.Add(Me.PictureBoxMainJoueur2)
        Me.Controls.Add(Me.PictureBoxMainJoueur1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.PictureBoxCasePlateau22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxMainJoueur6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxMainJoueur5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxMainJoueur4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxMainJoueur3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxMainJoueur2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxMainJoueur1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxEchange, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxDechet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBoxCasePlateau15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBoxMainJoueur1 As PictureBox
    Friend WithEvents PictureBoxMainJoueur2 As PictureBox
    Friend WithEvents PictureBoxMainJoueur3 As PictureBox
    Friend WithEvents PictureBoxMainJoueur4 As PictureBox
    Friend WithEvents PictureBoxMainJoueur5 As PictureBox
    Friend WithEvents PictureBoxMainJoueur6 As PictureBox
    Friend WithEvents PictureBoxCasePlateau11 As PictureBox
    Friend WithEvents PictureBoxCasePlateau21 As PictureBox
    Friend WithEvents PictureBoxCasePlateau12 As PictureBox
    Friend WithEvents PictureBoxCasePlateau22 As PictureBox
    Friend WithEvents PictureBoxEchange As PictureBox
    Friend WithEvents PictureBoxDechet1 As PictureBox
    Friend WithEvents PictureBoxCasePlateau31 As PictureBox
    Friend WithEvents PictureBoxCasePlateau41 As PictureBox
    Friend WithEvents PictureBoxCasePlateau51 As PictureBox
    Friend WithEvents PictureBoxCasePlateau32 As PictureBox
    Friend WithEvents PictureBoxCasePlateau42 As PictureBox
    Friend WithEvents PictureBoxCasePlateau52 As PictureBox
    Friend WithEvents PictureBoxCasePlateau54 As PictureBox
    Friend WithEvents PictureBoxCasePlateau44 As PictureBox
    Friend WithEvents PictureBoxCasePlateau34 As PictureBox
    Friend WithEvents PictureBoxCasePlateau53 As PictureBox
    Friend WithEvents PictureBoxCasePlateau43 As PictureBox
    Friend WithEvents PictureBoxCasePlateau33 As PictureBox
    Friend WithEvents PictureBoxCasePlateau24 As PictureBox
    Friend WithEvents PictureBoxCasePlateau14 As PictureBox
    Friend WithEvents PictureBoxCasePlateau23 As PictureBox
    Friend WithEvents PictureBoxCasePlateau13 As PictureBox
    Friend WithEvents PictureBoxCasePlateau55 As PictureBox
    Friend WithEvents PictureBoxCasePlateau45 As PictureBox
    Friend WithEvents PictureBoxCasePlateau35 As PictureBox
    Friend WithEvents PictureBoxCasePlateau25 As PictureBox
    Friend WithEvents PictureBoxCasePlateau15 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents LabelX As Label
    Friend WithEvents LabelY As Label
    Friend WithEvents BtnValider As Button
    Friend WithEvents BtnAnnuler As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
End Class
