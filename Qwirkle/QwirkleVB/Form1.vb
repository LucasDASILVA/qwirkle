﻿Public Class Form1
    'Deux booléens permettant de faire en sortant que le joueur puisse soit 
    'uniquement jouer soit uniquement échanger des tuiles de sa main
    Dim JoueurJoue As Boolean = 0
    Dim JoueurEchange As Boolean = 0

    Dim TableauMainsJoueur1(6) As Image 'Tableau comportant la Main du joueur 1
    Dim NomDeLaCaseJouee As String 'Nom de la case ou le joueur vient de jouer 
    Dim XTuile(5) As Integer ' Désigne le X De la Tuile(x) jouée par le joueur // (IndexTuile) ira dans la parenthèse
    Dim YTuile(5) As Integer 'Désigne le Y De la Tuile(x) jouée par le joueur // (IndexTuile) ira dans la parenthèse
    Dim IndexTuile As Integer = 0 'Permet de savoir combien de tuile le joueur à déja jouer pendant son tour
    Dim TuilePosee As Integer = 0 'Permet de savoir Combien de tuile le joueur à joué aussi mais nous avons besoin de deux variables 
    Dim listTuilesEnMoinsDuJoueur As List(Of Image) 'A chaque fois que le joueur joue une tuile on la met dans cette liste
    Dim listTuilesJouees As List(Of Image) 'A chaque fois que le joueur clic sur validé on recupere les tuiles jouées et validées

    '====== Partie Drag and Drop dans la main du joueur ======'
    '====== Partie Drag and Drop dans la main du joueur ======'
    '====== Partie Drag and Drop dans la main du joueur ======'
    '====== Partie Drag and Drop dans la main du joueur ======'
    '====== Partie Drag and Drop dans la main du joueur ======'

    Private TuileSelectionnee As Image
    Private TuileDep As PictureBox

    Private Sub PictureBoxMainJoueur_DragEnter(sender As Object, e As DragEventArgs) Handles PictureBoxMainJoueur1.DragEnter, PictureBoxMainJoueur6.DragEnter, PictureBoxMainJoueur5.DragEnter, PictureBoxMainJoueur4.DragEnter, PictureBoxMainJoueur3.DragEnter, PictureBoxMainJoueur2.DragEnter
        Dim Tuile As PictureBox = sender
        If e.Data.GetDataPresent(DataFormats.Bitmap) = True AndAlso Tuile.Image IsNot Nothing Then
            e.Effect = DragDropEffects.Move
            TuileSelectionnee = CType(sender, PictureBox).Image.Clone

        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub


    Private Sub PictureBoxMainJoueur_DragDrop(sender As Object, e As DragEventArgs) Handles PictureBoxMainJoueur1.DragDrop, PictureBoxMainJoueur6.DragDrop, PictureBoxMainJoueur5.DragDrop, PictureBoxMainJoueur4.DragDrop, PictureBoxMainJoueur3.DragDrop, PictureBoxMainJoueur2.DragDrop
        Dim Tuile As PictureBox = sender
        TuileDep.Image = Tuile.Image
        Tuile.Image = e.Data.GetData(DataFormats.Bitmap)

    End Sub

    Private Sub PictureBoxMainJoueur_MouseMove(sender As Object, e As MouseEventArgs) Handles PictureBoxMainJoueur1.MouseMove, PictureBoxMainJoueur6.MouseMove, PictureBoxMainJoueur5.MouseMove, PictureBoxMainJoueur4.MouseMove, PictureBoxMainJoueur3.MouseMove, PictureBoxMainJoueur2.MouseMove
        Dim EffetRealise As DragDropEffects
        Dim Tuile As PictureBox = sender
        sender.AllowDrop = False
        TuileDep = sender
        If e.Button = MouseButtons.Left AndAlso Tuile.Image IsNot Nothing Then
            EffetRealise = Tuile.DoDragDrop(Tuile.Image, DragDropEffects.Move)
            'If EffetRealise = DragDropEffects.Move Then
            '    Tuile.Image = TuileSelectionnee
            'End If

        End If
        sender.AllowDrop = True
        TuileDep = Nothing
        ' TuileSelectionnee = Nothing

    End Sub

    Private Sub Form1_LoadMain(sender As Object, e As EventArgs) Handles MyBase.Load
        Plateau.Close()
        For i As Byte = 1 To 6
            Me.Controls("PictureBoxMainJoueur" & i).AllowDrop = True
        Next

        'affichage d'une fausse main de joueur 

        TableauMainsJoueur1(0) = QwirkleVB.My.Resources.Resources.Rouge
        TableauMainsJoueur1(1) = QwirkleVB.My.Resources.Resources.Bleu
        TableauMainsJoueur1(2) = QwirkleVB.My.Resources.Resources.Jaune
        TableauMainsJoueur1(3) = QwirkleVB.My.Resources.Resources.Orange
        TableauMainsJoueur1(4) = QwirkleVB.My.Resources.Resources.Vert
        TableauMainsJoueur1(5) = QwirkleVB.My.Resources.Resources.Violet

        PictureBoxMainJoueur1.Image = TableauMainsJoueur1(0)  'Comment faire dans un for?
        PictureBoxMainJoueur2.Image = TableauMainsJoueur1(1)
        PictureBoxMainJoueur3.Image = TableauMainsJoueur1(2)
        PictureBoxMainJoueur4.Image = TableauMainsJoueur1(3)
        PictureBoxMainJoueur5.Image = TableauMainsJoueur1(4)
        PictureBoxMainJoueur6.Image = TableauMainsJoueur1(5)
        'Penser à récuperer la main du joueur à la fin de son tour pour la remplir en faisant appel à la fonction de lucas

    End Sub



    '====== Partie Drag and Drop de la main vers le plateau ======'
    '====== Partie Drag and Drop de la main vers le plateau ======'
    '====== Partie Drag and Drop de la main vers le plateau ======'
    '====== Partie Drag and Drop de la main vers le plateau ======'
    '====== Partie Drag and Drop de la main vers le plateau ======'
    Private Sub Form1_LoadPlateau(sender As Object, e As EventArgs) Handles MyBase.Load
        For IndiceLigne As Byte = 1 To 5
            For IndiceColonne As Byte = 1 To 5
                Me.Controls("PictureBoxCasePlateau" & IndiceColonne & IndiceLigne).AllowDrop = False
            Next
        Next
        Me.Controls("PictureBoxCasePlateau33").AllowDrop = True
    End Sub

    Private Sub PictureBoxCasePlateau_DragEnter(sender As Object, e As DragEventArgs) Handles PictureBoxCasePlateau11.DragEnter, PictureBoxCasePlateau22.DragEnter, PictureBoxCasePlateau21.DragEnter, PictureBoxCasePlateau12.DragEnter, PictureBoxCasePlateau55.DragEnter, PictureBoxCasePlateau54.DragEnter, PictureBoxCasePlateau53.DragEnter, PictureBoxCasePlateau52.DragEnter, PictureBoxCasePlateau51.DragEnter, PictureBoxCasePlateau45.DragEnter, PictureBoxCasePlateau44.DragEnter, PictureBoxCasePlateau43.DragEnter, PictureBoxCasePlateau42.DragEnter, PictureBoxCasePlateau41.DragEnter, PictureBoxCasePlateau35.DragEnter, PictureBoxCasePlateau34.DragEnter, PictureBoxCasePlateau33.DragEnter, PictureBoxCasePlateau32.DragEnter, PictureBoxCasePlateau31.DragEnter, PictureBoxCasePlateau25.DragEnter, PictureBoxCasePlateau24.DragEnter, PictureBoxCasePlateau23.DragEnter, PictureBoxCasePlateau15.DragEnter, PictureBoxCasePlateau14.DragEnter, PictureBoxCasePlateau13.DragEnter, MyBase.DragEnter
        Dim Tuile As PictureBox = sender
        If e.Data.GetDataPresent(DataFormats.Bitmap) = True And JoueurEchange = 0 And Tuile.Image Is Nothing Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub PictureBoxCasePlateau_DragDrop(sender As Object, e As DragEventArgs) Handles PictureBoxCasePlateau11.DragDrop, PictureBoxCasePlateau22.DragDrop, PictureBoxCasePlateau21.DragDrop, PictureBoxCasePlateau12.DragDrop, PictureBoxCasePlateau55.DragDrop, PictureBoxCasePlateau54.DragDrop, PictureBoxCasePlateau53.DragDrop, PictureBoxCasePlateau52.DragDrop, PictureBoxCasePlateau51.DragDrop, PictureBoxCasePlateau45.DragDrop, PictureBoxCasePlateau44.DragDrop, PictureBoxCasePlateau43.DragDrop, PictureBoxCasePlateau42.DragDrop, PictureBoxCasePlateau41.DragDrop, PictureBoxCasePlateau35.DragDrop, PictureBoxCasePlateau34.DragDrop, PictureBoxCasePlateau33.DragDrop, PictureBoxCasePlateau32.DragDrop, PictureBoxCasePlateau31.DragDrop, PictureBoxCasePlateau25.DragDrop, PictureBoxCasePlateau24.DragDrop, PictureBoxCasePlateau23.DragDrop, PictureBoxCasePlateau15.DragDrop, PictureBoxCasePlateau14.DragDrop, PictureBoxCasePlateau13.DragDrop, MyBase.DragDrop
        Dim Tuile As PictureBox = sender
        sender.AllowDrop = False
        TuileDep.Image = Tuile.Image
        Tuile.Image = e.Data.GetData(DataFormats.Bitmap)
        JoueurJoue = 1
        'Permet de connaitre l'endroit où on à joué
        NomDeLaCaseJouee = Tuile.Name
        Label1.Text = NomDeLaCaseJouee

        ' Ceci sera à améliorer mais en attendant on fait comme ça car nous sommes en retard 
        ' Cela permet de savoir où on joue et récuperer les coordonnées
        Select Case NomDeLaCaseJouee
            'Colone 1
            Case "PictureBoxCasePlateau11"
                YTuile(IndexTuile) = 1
                XTuile(IndexTuile) = 1
            Case "PictureBoxCasePlateau12"
                YTuile(IndexTuile) = 2
                XTuile(IndexTuile) = 1
            Case "PictureBoxCasePlateau13"
                YTuile(IndexTuile) = 3
                XTuile(IndexTuile) = 1
            Case "PictureBoxCasePlateau14"
                YTuile(IndexTuile) = 4
                XTuile(IndexTuile) = 1
            Case "PictureBoxCasePlateau15"
                YTuile(IndexTuile) = 5
                XTuile(IndexTuile) = 1

            'Colone 2
            Case "PictureBoxCasePlateau21"
                YTuile(IndexTuile) = 1
                XTuile(IndexTuile) = 2
            Case "PictureBoxCasePlateau22"
                YTuile(IndexTuile) = 2
                XTuile(IndexTuile) = 2
            Case "PictureBoxCasePlateau23"
                YTuile(IndexTuile) = 3
                XTuile(IndexTuile) = 2
            Case "PictureBoxCasePlateau24"
                YTuile(IndexTuile) = 4
                XTuile(IndexTuile) = 2
            Case "PictureBoxCasePlateau25"
                YTuile(IndexTuile) = 5
                XTuile(IndexTuile) = 2

                'Colone 3
            Case "PictureBoxCasePlateau31"
                YTuile(IndexTuile) = 1
                XTuile(IndexTuile) = 3
            Case "PictureBoxCasePlateau32"
                YTuile(IndexTuile) = 2
                XTuile(IndexTuile) = 3
            Case "PictureBoxCasePlateau33"
                YTuile(IndexTuile) = 3
                XTuile(IndexTuile) = 3
            Case "PictureBoxCasePlateau34"
                YTuile(IndexTuile) = 4
                XTuile(IndexTuile) = 3
            Case "PictureBoxCasePlateau35"
                YTuile(IndexTuile) = 5
                XTuile(IndexTuile) = 3

                'Colone 4
            Case "PictureBoxCasePlateau41"
                YTuile(IndexTuile) = 1
                XTuile(IndexTuile) = 4
            Case "PictureBoxCasePlateau42"
                YTuile(IndexTuile) = 2
                XTuile(IndexTuile) = 4
            Case "PictureBoxCasePlateau43"
                YTuile(IndexTuile) = 3
                XTuile(IndexTuile) = 4
            Case "PictureBoxCasePlateau44"
                YTuile(IndexTuile) = 4
                XTuile(IndexTuile) = 4
            Case "PictureBoxCasePlateau45"
                YTuile(IndexTuile) = 5
                XTuile(IndexTuile) = 4

                'Colone 5
            Case "PictureBoxCasePlateau51"
                YTuile(IndexTuile) = 1
                XTuile(IndexTuile) = 5
            Case "PictureBoxCasePlateau52"
                YTuile(IndexTuile) = 2
                XTuile(IndexTuile) = 5
            Case "PictureBoxCasePlateau53"
                YTuile(IndexTuile) = 3
                XTuile(IndexTuile) = 5
            Case "PictureBoxCasePlateau54"
                YTuile(IndexTuile) = 4
                XTuile(IndexTuile) = 5
            Case "PictureBoxCasePlateau55"
                YTuile(IndexTuile) = 5
                XTuile(IndexTuile) = 5
        End Select
        LabelX.Text = XTuile(IndexTuile)
        LabelY.Text = YTuile(IndexTuile)

        IndexTuile += 1

        If IndexTuile = 1 Then 'Si la premiere tuile à été jouée, mettre allow drop tout autour
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 1) + 1 & YTuile(IndexTuile - 1)).AllowDrop = True
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 1) - 1 & YTuile(IndexTuile - 1)).AllowDrop = True
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 1) & YTuile(IndexTuile - 1) + 1).AllowDrop = True
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 1) & YTuile(IndexTuile - 1) - 1).AllowDrop = True
        End If

        'Si Le joueur à jouer deux tuiles, on regarde si il à joué en ligne ou en colone et on agit en fonction de ceci
        'Ici c'est sur l'axe des X
        If ((IndexTuile >= 2 And IndexTuile < 6 And (XTuile(0) = XTuile(1))) And (YTuile(IndexTuile - 1) < 5) And (YTuile(IndexTuile - 1) > 1)) Then 'X1 = X2
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 1) & YTuile(IndexTuile - 1) + 1).AllowDrop = True
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 1) & YTuile(IndexTuile - 1) - 1).AllowDrop = True
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 2) + 1 & YTuile(IndexTuile - 2)).AllowDrop = False
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 2) - 1 & YTuile(IndexTuile - 2)).AllowDrop = False
        End If

        'Ici c'est sur l'axe des Y
        If ((IndexTuile >= 2 And IndexTuile < 6 And (YTuile(0) = YTuile(1))) And (XTuile(IndexTuile - 1) < 5) And (XTuile(IndexTuile - 1) > 1)) Then ' Y1 = Y2
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 1) + 1 & YTuile(IndexTuile - 1)).AllowDrop = True
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 1) - 1 & YTuile(IndexTuile - 1)).AllowDrop = True
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 2) & YTuile(IndexTuile - 2) + 1).AllowDrop = False
            Me.Controls("PictureBoxCasePlateau" & XTuile(IndexTuile - 2) & YTuile(IndexTuile - 2) - 1).AllowDrop = False
        End If
        'listTuilesEnMoinsDuJoueur.Add(Tuile.Image)
    End Sub


    '====== Partie de la Main qui echange avec la Poubelle ======'
    '====== Partie de la Main qui echange avec la Poubelle ======'
    '====== Partie de la Main qui echange avec la Poubelle ======'
    '====== Partie de la Main qui echange avec la Poubelle ======'
    '====== Partie de la Main qui echange avec la Poubelle ======'

    Dim CaseEchange(6) As Image
    Dim IndexDeCaseEchange = 1 'Penser à remettre à 0 si bouton annuler

    Private Sub Form1_LoadPictureBoxEchange(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Controls("PictureBoxEchange").AllowDrop = True
    End Sub

    Private Sub PictureBoxEchange_DragDrop(sender As Object, e As DragEventArgs) Handles PictureBoxEchange.DragDrop
        Dim Tuile As PictureBox = sender
        TuileDep.Image = Tuile.Image
        Tuile.Image = e.Data.GetData(DataFormats.Bitmap)

        Tuile.Image = Tuile.Image
        CaseEchange(IndexDeCaseEchange) = Tuile.Image
        'IndexDeCaseEchange = IndexDeCaseEchange + 1 'A réactiver mais c'est utile pour voir le fonctionnement de l'echange avec la poubelle
        PictureBoxDechet1.Image = CaseEchange(1)
        Tuile.Image = Nothing 'On peut éventuellement mettre une image symbolisant un échange
        JoueurEchange = 1 'Penser à remettre à 0 si bouton annuler
        'listTuilesEnMoinsDuJoueur.Add(Tuile.Image) 'Ceci n'est pas encore vérifié
    End Sub

    Private Sub PictureBoxEchange_DragEnter(sender As Object, e As DragEventArgs) Handles PictureBoxEchange.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) = True And JoueurJoue = 0 Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub BtnValider_Click(sender As Object, e As EventArgs) Handles BtnValider.Click
        Throw New NotImplementedException()
        'listTuilesJouees = listTuilesEnMoinsDuJoueur 'Penser a implémenter le fait de mettre les tuiles du joueur la dedans
        'IndexTuile = 0
        'For i As Byte = 1 To 6
        '   If TableauMainsJoueur(IndexJoueur) is nothing then
        '   PerteTuile()
        '   EndIf
        'Next
        'IndexJoueur += 1
        'if IndexJoueur > 4 then
        '   IndexJoueur = 1
        'EndIf
        'Mettre le AllowDrop a True partout autour des cases pleines si on peut jouer à coté (forme ou couleur = forme ou couleur and si couleur and forme <> couleur and forme (si ce ne sont pas deux tuiles strictement pareil)
        'JoueurEchange = 0
        'JoueurJoue = 0
    End Sub

    Private Sub BtnAnnuler_Click(sender As Object, e As EventArgs) Handles BtnAnnuler.Click
        Throw New NotImplementedException()
        'listTuilesEnMoinsDuJoueur à remettre dans la main du joueur
        'JoueurEchange = 0
        'JoueurJoue = 0
        'IndexTuile = 0
        'Vider listTuilesJouees
    End Sub

End Class