﻿Imports QwirkleLib
Public Class Form2

    Dim zoom As Integer = 100

    Dim Main(5) As tuile

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim nb As Integer = 0
        Dim nom As String

        For J As Integer = 0 To 30 'axes y des colonnes
            For I As Integer = 0 To 30 'axe x des lignes
                Dim img As PictureBox 'PictureBox passe sous le nom img

                Dim newimg As New PictureBox 'Les nouvelles PictureBox passe sous le nom newimg
                newimg.BorderStyle = BorderStyle.FixedSingle 'modification de la bordure
                newimg.Name = "img" & nb & J & "," & I 'le nom de l'image prend img + un nombre qui s'incrémente 
                newimg.Tag = nb 'le texte dans l'image prend
                newimg.Size = New Size(12, 12) 'dimensionnement de l'image
                If I = 0 Then
                    newimg.Location = New Point(10 + (J * 12), 10) 'position image premiere ligne de toute colonne
                Else
                    ' nom = "img" & (nb - 1)
                    img = Me.Panel1.Controls(nb - 1)
                    newimg.Location = New Point(img.Location.X, img.Height + img.Location.Y) 'position des lignes suivante
                End If
                Me.Panel1.Controls.Add(newimg) 'Création de la pictureBox à l'écran
                nb += 1

            Next
        Next

        Dim Pioche As pioche
        Pioche = New pioche()
        Main(0) = New tuile(Pioche.Perte_tuile())
        Main(1) = New tuile(Pioche.Perte_tuile())
        Main(2) = New tuile(Pioche.Perte_tuile())
        Main(3) = New tuile(Pioche.Perte_tuile())
        Main(4) = New tuile(Pioche.Perte_tuile())
        Main(5) = New tuile(Forme.trefle, Couleur.vert)

        For i = 0 To 5
            Dim nomImg As String = Main(i).getForme_tuile.ToString & "_" & Main(i).getCouleur_tuile.ToString
            CType(Panel2.Controls("PictureBox" & i + 1), PictureBox).Image = My.Resources.ResourceManager.GetObject(nomImg)
        Next

    End Sub

    Private Sub Button_Menu_Click(sender As Object, e As EventArgs) Handles Button_Menu.Click
        Dim Msg, Style, Response, Mystring
        Msg = "Voulez-vous vraiment quitter ?"    ' Message.
        Style = vbYesNo ' Bouttons.

        Response = MsgBox(Msg, Style)
        If Response = vbYes Then    ' User chose oui.
            Mystring = "Oui"
            Me.Hide()
            Accueil.Show()
        Else    ' User chose non.
            Mystring = "Non"
        End If
    End Sub

    Private Sub Bouton_zoom_Click(sender As Object, e As EventArgs) Handles Bouton_zoom.Click 'zoom 
        Dim ratio As Single = 1.1
        Bouton_zoom.Enabled = True
        Panel1.Size = New Size(Panel1.Width * 1.1, Panel1.Height * 1.1) 'augmente la taille de 1.1
        zoom -= 20
    End Sub

    Private Sub Bouton_dezoom_Click(sender As Object, e As EventArgs) Handles Bouton_dezoom.Click 'dezoom
        Dim ratio As Single = 0.9
        Bouton_dezoom.Enabled = True
        Panel1.Size = New Size(Panel1.Width * 0.9, Panel1.Height * 0.9) 'multiplie la taille de 0.9
        zoom += 20
    End Sub


End Class