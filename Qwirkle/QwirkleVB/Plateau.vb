﻿Imports QwirkleVB
Imports QwirkleLib
Public Class Plateau
    Dim RefTour As Integer = 1 'Variable référence de l'ordre de jeu
    Dim NBTour As Integer = 1 'Compte le nombre de tour
    Public Order As Integer = 1
    Public nbstart As Boolean = True 'Pour afficher juste une fois la boite de dialogue
    Dim nb As Integer = 0
    Dim Main(5) As tuile

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label5.Text = Joueur1.Get_Name()
        Label_NameJ1.Text = Joueur1.Get_Name() + " : " '+ Joueur1.Get_Score
        Label_NameJ2.Text = Joueur2.Get_Name() + " : " '+ Joueur2.Get_Score
        If NbJoueurs.NBJoueurSelect = 3 Then
            Label_NameJ3.Text = Joueur3.Get_Name() + " : " '+ Joueur3.Get_Score
        ElseIf NbJoueurs.NBJoueurSelect = 4 Then
            Label_NameJ3.Text = Joueur3.Get_Name() + " : " '+ Joueur3.Get_Score
            Label_NameJ4.Text = Joueur4.Get_Name() + " : " '+ Joueur4.Get_Score
        End If

        For J As Integer = 0 To 29 'axes y des colonnes
            For I As Integer = 0 To 29 'axe x des lignes
                Dim img As PictureBox 'PictureBox passe sous le nom img

                Dim newimg As New PictureBox 'Les nouvelles PictureBox passe sous le nom newimg
                newimg.BorderStyle = BorderStyle.FixedSingle 'modification de la bordure
                newimg.Name = "img" & J & "," & I 'le nom de l'image prend img + coordonnée
                newimg.Tag = I & "," & J 'Chaque tuile prend un tag de coordonnée référenciel


                newimg.Size = New Size(20, 20) 'dimensionnement de l'image + modifier la valeur dans le newimg.location à cote du J
                If I = 0 Then
                    newimg.Location = New Point(10 + (J * 20), 10) 'position image premiere ligne de toute colonne
                Else

                    img = Me.Panel1.Controls(nb - 1)
                    newimg.Location = New Point(img.Location.X, img.Height + img.Location.Y) 'position des lignes suivante
                End If
                Me.Panel1.Controls.Add(newimg) 'Création de la pictureBox à l'écran
                nb += 1

            Next
        Next

        Dim Pioche As pioche
        Pioche = New pioche()
        Main(0) = New tuile(Pioche.Perte_tuile())
        Main(1) = New tuile(Pioche.Perte_tuile())
        Main(2) = New tuile(Pioche.Perte_tuile())
        Main(3) = New tuile(Pioche.Perte_tuile())
        Main(4) = New tuile(Pioche.Perte_tuile())
        Main(5) = New tuile(Pioche.Perte_tuile())

        For i = 0 To 5
            Dim nomImg As String = Main(i).getForme_tuile.ToString & "_" & Main(i).getCouleur_tuile.ToString
            CType(Panel2.Controls("PictureBox" & i + 1), PictureBox).Image = My.Resources.ResourceManager.GetObject(nomImg)
        Next

        Panel1.Width = Panel1.Controls("img29,29").Location.X + Panel1.Controls("img29,29").Width + 14

        Panel1.Height = Panel1.Controls("img29,29").Location.Y + Panel1.Controls("img29,29").Height + 14
        'Me.AutoScroll = True

    End Sub


    Private Sub Form2_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove


        If nbstart = True Then
            Dialogscore.Show()
            nbstart = False
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        RefTour = RefTour + 1

        If RefTour = NbJoueurs.NBJoueurSelect + 1 Then
            NBTour = NBTour + 1
            RefTour = 1
            Label_NbTour.Text = NBTour
        End If
        Select Case RefTour
            Case 1
                'Main Joueur 1
                Label5.Text = Joueur1.Get_Name()
            Case 2
                'Main Joueur 2
                Label5.Text = Joueur2.Get_Name()
            Case 3
                'Main Joueur 3
                Label5.Text = Joueur3.Get_Name()
            Case 4
                'Main Joueur 4
                Label5.Text = Joueur4.Get_Name()
        End Select

    End Sub

    Private Sub Panel1_MouseMove(sender As Object, e As MouseEventArgs) Handles Panel1.MouseMove

        If nbstart = True Then
            Dialogscore.Show()
            nbstart = False
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Form1.ShowDialog()
        Me.Close()
    End Sub
End Class